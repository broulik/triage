/*
 * This file is part of the KDE Triage Project
 * Copyright (C) 2016 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QObject>

#include <QDBusMessage>
#include <QUrl>

#include "triage_export.h"

class QQuickItem;

namespace Triage {

class TRIAGE_EXPORT Adaptor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQuickItem *source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(bool opened READ opened WRITE setOpened NOTIFY openedChanged)

public:
    Adaptor(QObject *parent = nullptr);
    virtual ~Adaptor();

    QQuickItem *source() const;
    void setSource(QQuickItem *source);

    QUrl url() const;
    void setUrl(const QUrl &url);

    bool opened() const;
    void setOpened(bool opened);

    Q_INVOKABLE void open();
    Q_INVOKABLE void toggle();
    Q_INVOKABLE void close();

signals:
    void sourceChanged(QQuickItem *source);
    void urlChanged(const QUrl &url);
    void openedChanged(bool opened);

private:
    static QDBusMessage createMessage(const QString &method);
    QRect absoluteGeometry(QQuickItem *item) const;

    QQuickItem *m_source = nullptr;
    QUrl m_url;
    bool m_opened = false;

};

}
