/*
 * This file is part of the KDE Triage Project
 * Copyright (C) 2016 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QQuickItem>
#include <QQmlParserStatus>

#include <QUrl>

#include "triage_export.h"

namespace Triage {

class PreviewPlugin;

class TRIAGE_EXPORT Preview : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QString mimetype READ mimetype WRITE setMimetype)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString highlight READ highlight WRITE setHighlight)
    Q_PROPERTY(bool loaded READ loaded)

    // TODO status

public:
    Preview(QQuickItem* parent = 0);
    virtual ~Preview();

    void componentComplete() override;

    QString mimetype() const;
    void setMimetype(const QString& mime);

    QUrl url() const;
    void setUrl(const QUrl &url);

    QString highlight() const;
    void setHighlight(const QString& highlight);

    bool loaded() const { return m_loaded; }


signals:
    void loadingFinished();

    void urlChanged(const QUrl &url);

public slots:
    //void refresh();
    void clear();

private slots:
    //void slotPreviewGenerated(QQuickItem* item);

    //void setPluginContexts();

private:
    void createPreview();
    QString libraryPath() const;

    bool m_loaded = false; // TODO replace by status enum
    QString m_mimetype;
    QUrl m_url;
    QString m_highlight;

    QString m_oldMimetype;
    QString m_oldUrl;

    QString m_libraryPath;

    PreviewPlugin *m_previewPlugin = nullptr;
    QQuickItem *m_previewItem = nullptr;

    //QVector<PreviewPlugin*> m_plugins;
    //QVector<PreviewPlugin*> allPlugins();
    //PreviewPlugin* m_filePlugin;

};

}
