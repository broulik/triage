/*
 * This file is part of the KDE Triage Project
 * Copyright (C) 2016 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "adaptor.h"

#include <QDBusConnection>
#include <QDBusPendingCall>
#include <QPoint>
#include <QQuickItem>
#include <QQuickWindow>
#include <QWindow>

using namespace Triage;

Adaptor::Adaptor(QObject *parent) : QObject(parent)
{

}

Adaptor::~Adaptor() = default;

QQuickItem *Adaptor::source() const
{
    return m_source;
}

void Adaptor::setSource(QQuickItem *source)
{
    if (m_source != source) {
        m_source = source;
        emit sourceChanged(source);
    }
}

QUrl Adaptor::url() const
{
    return m_url;
}

void Adaptor::setUrl(const QUrl &url)
{
    if (m_url != url) {
        m_url = url;
        emit urlChanged(url);
    }
}

bool Adaptor::opened() const
{
    return m_opened;
}

void Adaptor::setOpened(bool opened)
{
    if (m_opened) {
        close();
    } else {
        open();
    }
}

void Adaptor::open()
{
    const QRect geo = absoluteGeometry(m_source);
    qDebug() << "Open geo" << geo;
    if (!geo.isValid()) {
        qWarning() << "Cannot open preview without source or source with invalid geometry";
        return;
    }

    auto msg = createMessage(QStringLiteral("preview"));

    msg.setArguments({m_url.toString(), geo.x(), geo.y(), geo.width(), geo.height()});

    QDBusConnection::sessionBus().asyncCall(msg);

    // FIXME instead connect to the property on dbus which we'll update when the
    // preview closes due to other means
    m_opened = true;
    emit openedChanged(m_opened);
}

void Adaptor::toggle()
{

}

void Adaptor::close()
{
    QDBusConnection::sessionBus().asyncCall(createMessage(QStringLiteral("hidePreview")));

    // FIXME instead connect to the property on dbus which we'll update when the
    // preview closes due to other means
    m_opened = false;
    emit openedChanged(m_opened);
}

QDBusMessage Adaptor::createMessage(const QString &method)
{
    return QDBusMessage::createMethodCall(
        QStringLiteral("org.kde.triage"),
        QStringLiteral("/"),
        QStringLiteral("org.kde.triage"),
        method
    );
}

QRect Adaptor::absoluteGeometry(QQuickItem *item) const
{
    if (!item || !item->window()) {
        return QRect();
    }

    // TODO wayland :)
    const QPoint scenePos = item->mapToScene(QPointF(0,0)).toPoint();
    qDebug() << "scene pos";
    return QRect(item->window()->mapToGlobal(scenePos), QSize(item->width(), item->height()));
}
