project(triage-preview-image)

set (
   triage_preview_image_SRCS
   imageplugin.cpp
)

kcoreaddons_add_plugin(
   triage_preview_image
   JSON triage-preview-image.json
   SOURCES ${triage_preview_image_SRCS}
   INSTALL_NAMESPACE ${TRIAGE_PLUGIN_DIR}
   )

target_link_libraries(triage_preview_image
      triage
   Qt5::Core
Qt5::Qml

#      KF5::KIOWidgets
      Qt5::Quick
)
