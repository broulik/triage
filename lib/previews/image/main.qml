import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0 as PlasmaCore

//import org.kde.triage 1.0 as Triage

ColumnLayout {
    id: preview

    implicitWidth: 300
    implicitHeight: 300
    //Layout.preferredWidth: image.sourceSize.width
    //Layout.preferredHeight: image.sourceSize.height

    property alias source: image.source

    Image {
        id: image
        Layout.fillWidth: true
        Layout.fillHeight: true
        asynchronous: true
    }


}
