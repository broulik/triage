﻿/*
 * This file is part of the KDE Triage Project
 * Copyright (C) 2016 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "textplugin.h"

#include <QDebug>

#include <QQmlContext>

TRIAGE_EXPORT_PREVIEW(TextPlugin, "triage-preview-text.json")

TextPlugin::TextPlugin(QObject* parent, const QVariantList &): PreviewPlugin(parent)
{
}

void TextPlugin::createPreview()
{
    QQmlComponent component(context()->engine(), "/home/kaiuwe/Projekte/triage/lib/previews/text/main.qml", this);
    if (component.status() != QQmlComponent::Ready) {
        qWarning() << "error loading component for file" << component.errorString();
        emit previewFailed();
        return;
    }

    QQuickItem *item = qobject_cast<QQuickItem *>(component.create());
    if (!item) {
        qWarning() << "Failed to create instance of preview";
        emit previewFailed();
        return;
    }

    updatePreview(item);

    emit previewGenerated(item);
}

void TextPlugin::updatePreview(QQuickItem *item)
{
    // FIXME no, just no... ;)
    QFile file(url().toLocalFile());
    if (!file.open(QIODevice::ReadOnly)) {
        emit previewFailed();
        return;
    }

    item->setProperty("text", QString::fromUtf8(file.readAll()));
    emit previewUpdated(item);
}

#include "textplugin.moc"
