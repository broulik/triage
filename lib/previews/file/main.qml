import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0 as PlasmaCore

//import org.kde.triage 1.0 as Triage

ColumnLayout {
    id: preview

    property string name
    property string icon
    property string typeName

    PlasmaCore.IconItem {
        Layout.fillWidth: true
        Layout.preferredHeight: width
        Layout.maximumHeight: preview.parent.height / 2
        source: preview.icon
    }

    Label {
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
        text: preview.name
    }

    Label {
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        opacity: 0.6
        text: preview.typeName
    }

    Rectangle {
        Layout.fillWidth: true
        height: 1
        color: "black"
    }

    Item {
        Layout.fillWidth: true
        Layout.fillHeight: true
    }

}
