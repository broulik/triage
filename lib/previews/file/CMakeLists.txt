project(triage-preview-file)

set (
   triage_preview_file_SRCS
   fileplugin.cpp
)

kcoreaddons_add_plugin(
   triage_preview_file
   JSON triage-preview-file.json
   SOURCES ${triage_preview_file_SRCS}
   INSTALL_NAMESPACE ${TRIAGE_PLUGIN_DIR}
   )

target_link_libraries(triage_preview_file
      triage
   Qt5::Core
Qt5::Qml

#      KF5::KIOWidgets
      Qt5::Quick
#     ${BALOO_CORE_LIBRARY} ${BALOO_FILE_LIBRARY}
)

# install(TARGETS triagefileplugin DESTINATION ${PLUGIN_INSTALL_DIR})
