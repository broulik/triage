/*
 * This file is part of the KDE Triage Project
 * Copyright (C) 2016 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QObject>
#include <QQmlParserStatus>

#include <QUrl>

#include "triage_export.h"

class QStandardItemModel;

namespace Triage {

class TRIAGE_EXPORT Metadata : public QObject
{
    Q_OBJECT

    /**
     * @brief The file to query metadata from
     */
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)

    /**
     * @brief The service to query metadata from
     */
    //Q_PROPERTY(QString storageId READ storageId WRITE setStorageId NOTIFY storageIdChanged)

    /**
     * @brief The filename or application title
     */
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)

    /**
     * @brief "Open With" applications
     *
     * This model contains applications that could open this file
     */
    Q_PROPERTY(QStandardItemModel *openWithApps READ openWithApps CONSTANT)
    /**
     * @brief Application's recently opened documents
     *
     * This model contains recently opened documents, if @c url points
     * to an application
     */
    Q_PROPERTY(QStandardItemModel *recentDocuments READ recentDocuments CONSTANT)
    /**
     * @brief Jump List actions
     *
     * This model contains jump list actions, if @c url points to an
     * application service
     */
    Q_PROPERTY(QStandardItemModel *actions READ actions CONSTANT)

    /**
     * @brief Various information about the file
     *
     * This can be shown in a table and contains information such as
     * file name, file size, location, image size, artist and author information, etc
     */
    Q_PROPERTY(QStandardItemModel *metadata READ metadata CONSTANT)

    /**
     * @brief Tags associated with this file
     */
    Q_PROPERTY(QStandardItemModel *tags READ tags CONSTANT)

    // TODO purpose share targets?

    /**
     * @brief The rating of this file
     *
     * From 0 to 5 or -1 if no rating is set
     */
    Q_PROPERTY(qreal rating READ rating NOTIFY ratingChanged)
    /**
     * @brief The comment of this file
     */
    Q_PROPERTY(QString comment READ comment NOTIFY commentChanged)

    // TODO status

public:
    explicit Metadata(QObject *parent = nullptr);
    virtual ~Metadata();

    enum class OpenWithAppsRoles {
        StorageId = Qt::UserRole
    };

    enum class MetadataRoles {
        Text = Qt::UserRole, ///< a formatted string that can be shown to the user
        Raw, ///< (optional) the raw data, this might be eg. a QDateTime that text has pretty-formatted
    };

    QUrl url() const;
    void setUrl(const QUrl &url);

    QString title() const;

    QStandardItemModel *openWithApps() const;
    QStandardItemModel *recentDocuments() const;
    QStandardItemModel *actions() const;
    QStandardItemModel *metadata() const;
    QStandardItemModel *tags() const;

    qreal rating() const;
    QString comment() const;

signals:
    void urlChanged(const QUrl &url);

    void titleChanged(const QString &title);

    void ratingChanged(qreal rating);
    void commentChanged(const QString &comment);

private:
    void loadMetadata();

    QUrl m_url;

    QString m_title;
    QStandardItemModel *m_openWithApps;
    QStandardItemModel *m_recentDocuments;
    QStandardItemModel *m_actions;
    QStandardItemModel *m_metadata;
    QStandardItemModel *m_tags;
    qreal m_rating = -1;
    QString m_comment;

};

}
