/*
 * This file is part of the KDE Triage Project
 * Copyright (C) 2016 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "preview.h"
#include "previewplugin.h"

#include <KService>
#include <KPluginTrader>
#include <KPluginInfo>
#include <QDebug>

#include <QTimer>

#include <QQmlContext>
#include <QFileInfo> // todo kio stab job
#include <QMimeDatabase>

using namespace Triage;

Preview::Preview(QQuickItem* parent)
    : QQuickItem(parent)
{
    //setFlag(QGraphicsItem::ItemHasNoContents, false);

    // TODO load only the plugins that we care for

    /*m_plugins = allPlugins();
    foreach(PreviewPlugin* plugin, m_plugins) {
        connect(plugin, SIGNAL(previewGenerated(QQuickItem*)),
                this, SLOT(slotPreviewGenerated(QQuickItem*)));

        if (plugin->mimetypes().contains("file"))
            m_filePlugin = plugin;
    }

    // When the object is created, it doesn't have a QQmlContext
    QTimer::singleShot(0, this, SLOT(setPluginContexts()));*/

    // FIXME how can we propagate the Layout attached stuff to our parent?
    // so when the preview qml file does stuff like Layout.preferredWidth and stuff
    // we propagate that to our parent layout?
    connect(this, &QQuickItem::widthChanged, this, [this] {
        if (m_previewItem) {
            m_previewItem->setWidth(width());
        }
    });
    connect(this, &QQuickItem::heightChanged, this, [this] {
        if (m_previewItem) {
            m_previewItem->setHeight(height());
        }
    });
}

Preview::~Preview()
{
    //qDeleteAll(m_plugins);
    //m_plugins.clear();

    delete m_previewPlugin;
    m_previewPlugin = nullptr;

    // TODO delete preview item?
}

void Preview::componentComplete()
{
    QQuickItem::componentComplete();

    createPreview();
}

/*void Preview::setPluginContexts()
{
    QQmlContext* context = qmlContext(parentItem());
    foreach(PreviewPlugin* plugin, m_plugins) {
        plugin->setContext(context);
    }
}*/

QString Preview::libraryPath() const
{
    if (!m_url.isValid()) {
        return QString();
    }

    // TODO kio stat job?
    QFileInfo fileInfo(m_url.toLocalFile());
    if (!fileInfo.exists()) {
        qWarning() << "File does not exist" << fileInfo.absoluteFilePath();
        return QString();
    }

    QMimeDatabase db;
    const QMimeType mime = db.mimeTypeForFile(m_url.toLocalFile());

    if (!mime.isValid()) {
        qWarning() << "Failed to determin mime" << m_url;
        return QString();
    }

    const auto serviceList = KPluginTrader::self()->query(QStringLiteral("org.kde.triage"));
    qDebug() << "FOUND" << serviceList.count();

    // urgh...
    QStringList exactMatchCandidates;
    QStringList anyOfMatchCandidates;
    QStringList wildcardCandidates;

    for (const auto &plugin : serviceList) {
        // it uses the following order for the plugins, eg. for image/png:
        // - plugin that only and really only supports that type (eg. only image/png, nothing else)
        // - plugin that supports this among others (eg. has image/png, application/foo, image/jpeg)
        // - plugin that supports this mime group (eg. image/*)
        // - if all fails use the generic file plugin, see after the loop

        // TODO is there no easier way to get that?
        const QVariantList &mimeTypes = plugin.properties().value(QStringLiteral("KPlugin")).toMap().value(QStringLiteral("MimeTypes")).toList();

        if (mimeTypes.count() == 1 && mimeTypes.first().toString() == mime.name()) {
            exactMatchCandidates << plugin.libraryPath();
            // this condition has topmost priority, so if we found one we can just exit the loop
            // FIXME do we still need a QStringList then?!
            break;
        }

        auto mimeTypesBegin = mimeTypes.constBegin();
        auto mimeTypesEnd = mimeTypes.constEnd();

        auto it = std::find_if(mimeTypesBegin, mimeTypesEnd, [&mime](const QVariant &item) {
            return item.toString() == mime.name();
        });

        if (it != mimeTypesEnd) {
            anyOfMatchCandidates << plugin.libraryPath();
            continue;
        }

        auto jt = std::find_if(mimeTypesBegin, mimeTypesEnd, [&mime](const QVariant &item) {
            // TODO inherits?
            const QString &stringItem = item.toString();
            return stringItem.endsWith(QLatin1Char('/')) && mime.name().startsWith(stringItem);
        });

        if (jt != mimeTypesEnd) {
            wildcardCandidates << plugin.libraryPath();
        }
    }

    qDebug() << "CANDIDATES";
    qDebug() << "  exact match" << exactMatchCandidates;
    qDebug() << "  any of match" << anyOfMatchCandidates;
    qDebug() << "  wildcard match" << wildcardCandidates;

    if (!exactMatchCandidates.isEmpty()) {
        return exactMatchCandidates.at(0);
    }

    if (!anyOfMatchCandidates.isEmpty()) {
        return anyOfMatchCandidates.at(0);
    }

    if (!wildcardCandidates.isEmpty()) {
        return wildcardCandidates.at(0);
    }

    // if all fails, use the generic plugin
    const auto genericPluginList = KPluginTrader::self()->query(QStringLiteral("org.kde.triage")/*, {},
                                                                // FIXME Doesnt work
                                                                QStringLiteral("Id == 'org.kde.Triage.FilePreview'")*/);
    // FIXME
    for (const auto &plugin : genericPluginList) {
        qDebug() << plugin.name();
        if (plugin.name() == QLatin1String("Generic Files")) {
            return plugin.libraryPath();
        }
    }

    /*if (genericPluginList.count() != 1) {
        qWarning() << "no generic file plugin found";
        return QString();
    }
    //Q_ASSERT(genericPluginList.count() == 1);

    const auto &filePlugin = genericPluginList.first();
    if (filePlugin.isValid()) {
        return filePlugin.libraryPath();
    }*/

    qDebug() << ":(";

    return QString();
}

void Preview::createPreview()
{
    const QString libPath = libraryPath();
    qDebug() << "create preview from" << libPath;

    if (libPath.isEmpty()) {
        // setStatus(Status::Error);
        return;
    }

    if (m_libraryPath != libPath || !m_previewPlugin || !m_previewItem) {
        m_libraryPath.clear(); // in case we return and don't load a preview
        delete m_previewPlugin;
        m_previewPlugin = nullptr;

        if (m_previewItem) {
            m_previewItem->deleteLater();
            m_previewItem = nullptr;
        }

        KPluginLoader loader(libPath);
        KPluginFactory *factory = loader.factory();
        if (!factory) {
            // setStatus(Status::Error);
            return;
        }

        m_previewPlugin = factory->create<PreviewPlugin>(this, QVariantList());
        if (!m_previewPlugin) {
            // setStatus(Status::Error);
            return;
        }

        m_previewPlugin->setContext(qmlContext(this));

        auto setupItem = [this](QQuickItem *item) {
            item->setParentItem(this);
            item->setWidth(width()); // TODO anchors.fill kind of thing?
            item->setHeight(height());

            setImplicitWidth(item->implicitWidth());
            setImplicitHeight(item->implicitHeight());

            connect(item, &QQuickItem::implicitWidthChanged, this, [this, item] {
                setImplicitWidth(item->implicitWidth());
            });
            connect(item, &QQuickItem::implicitHeightChanged, this, [this, item] {
                setImplicitHeight(item->implicitHeight());
            });

            m_previewItem = item;
        };

        connect(m_previewPlugin, &PreviewPlugin::previewGenerated, this, [this, setupItem](QQuickItem *item) {
            // TODO set status ready

            qDebug() << "GENERATED";

            setupItem(item);
        });

        connect(m_previewPlugin, &PreviewPlugin::previewUpdated, this, [this, setupItem](QQuickItem *item) {
            if (!m_previewItem) {
                return;
            }

            setupItem(item);
        });

        connect(m_previewPlugin, &PreviewPlugin::previewFailed, this, [this] {
            qDebug() << "PREVIEW FAILED";
            // setStatus error
        });

        m_previewPlugin->setUrl(m_url);
        m_previewPlugin->createPreview();

        m_libraryPath = libPath;
        return;
    }

    qDebug() << "update instead" << m_previewPlugin;

    m_previewPlugin->setUrl(m_url);
    m_previewPlugin->updatePreview(m_previewItem);

    // gosh this all is so horrible
    m_previewItem->setParentItem(this);
    m_previewItem->setWidth(width()); // TODO anchors.fill kind of thing?
    m_previewItem->setHeight(height());

    setImplicitWidth(m_previewItem->implicitWidth());
    setImplicitHeight(m_previewItem->implicitHeight());

/*

    QVector<PreviewPlugin*> plugins;

    foreach (const auto &s, serviceList) {
        qDebug() << s.pluginName() << s.name();

        KPluginLoader loader(s.libraryPath());
        KPluginFactory* factory = loader.factory();

        PreviewPlugin* p = factory->create<PreviewPlugin>(this, QVariantList());
        if (!p) {
//             qWarning() <<
            continue;
        }
        plugins << p;
    }*/
}

/*void Preview::refresh()
{
    if (m_oldUrl == m_url && m_oldMimetype == m_mimetype) {
        if (m_declarativeItem)
            emit loadingFinished();
        return;
    }

    m_loaded = false;

    bool foundPlugin = false;
    QUrl url = QUrl::fromLocalFile(m_url);
    foreach (PreviewPlugin* plugin, m_plugins) {
        foreach (const QString& mime, plugin->mimetypes()) {
            if (m_mimetype.startsWith(mime)) {
                plugin->setUrl(url);
                plugin->setMimetype(m_mimetype);
                plugin->setHighlight(m_highlight);
                plugin->generatePreview();

                foundPlugin = true;
                break;
            }
        }
    }

    if (!foundPlugin && m_filePlugin) {
        m_filePlugin->setUrl(url);
        m_filePlugin->setMimetype(m_mimetype);
        m_filePlugin->setHighlight(m_highlight);
        m_filePlugin->generatePreview();
    }
}*/

/*void Preview::slotPreviewGenerated(QQuickItem* item)
{
    clear();

    m_declarativeItem = item;
    item->setParentItem(this);

    setWidth(item->width());
    setHeight(item->height());

    m_loaded = true;
    emit loadingFinished();
}*/

void Preview::clear()
{
    if (m_previewItem) {
        m_previewItem->deleteLater();
        m_previewItem = nullptr;
    }
}

QString Preview::mimetype() const
{
    return m_mimetype;
}

void Preview::setMimetype(const QString& mime)
{
    if (m_mimetype != mime) {
        m_oldMimetype = m_mimetype;
        m_mimetype = mime;
    }
}

void Preview::setUrl(const QUrl &url)
{
    if (m_url != url) {
        m_url = url;
        emit urlChanged(url);

        if (isComponentComplete()) {
            createPreview();
        }
    }
}

QUrl Preview::url() const
{
    return m_url;
}

void Preview::setHighlight(const QString& highlight)
{
    m_highlight = highlight;
}

QString Preview::highlight() const
{
    return m_highlight;
}
