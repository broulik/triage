/*
 * This file is part of the KDE Triage Project
 * Copyright (C) 2016 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "metadata.h"

#include <QtQml>

#include <QStandardItemModel>

#include <KFormat>
#include <KLocalizedString>
#include <KMimeTypeTrader>

#include <KIOCore/KFileItem>

#include <KFileMetaData/PropertyInfo>
#include <KFileMetaData/Properties>

#include <QDebug>

using namespace Triage;

Metadata::Metadata(QObject *parent)
    : QObject(parent)
    , m_openWithApps(new QStandardItemModel(this))
    , m_recentDocuments(new QStandardItemModel(this))
    , m_actions(new QStandardItemModel(this))
    , m_metadata(new QStandardItemModel(this))
    , m_tags(new QStandardItemModel(this))
{

    m_openWithApps->setItemRoleNames({
        {Qt::DisplayRole, QByteArrayLiteral("title")},
        {Qt::DecorationRole, QByteArrayLiteral("iconName")},
        {static_cast<int>(OpenWithAppsRoles::StorageId), QByteArrayLiteral("storageId")}
    });

    m_metadata->setItemRoleNames({
        {Qt::DisplayRole, QByteArrayLiteral("title")},
        {Qt::DecorationRole, QByteArrayLiteral("iconName")},
        {static_cast<int>(MetadataRoles::Text), QByteArrayLiteral("text")}
        // TODO url
        // TODO allow multiple?
    });

}

Metadata::~Metadata() = default;

QUrl Metadata::url() const
{
    return m_url;
}

void Metadata::setUrl(const QUrl &url)
{
    if (m_url != url) {
        m_url = url;
        emit urlChanged(url);

        loadMetadata();
    }
}

QString Metadata::title() const
{
    return m_title;
}

QStandardItemModel *Metadata::openWithApps() const
{
    return m_openWithApps;
}

QStandardItemModel *Metadata::recentDocuments() const
{
    return m_recentDocuments;
}

QStandardItemModel *Metadata::actions() const
{
    return m_actions;
}

QStandardItemModel *Metadata::metadata() const
{
    return m_metadata;
}

QStandardItemModel *Metadata::tags() const
{
    return m_tags;
}

qreal Metadata::rating() const
{
    return m_rating;
}

QString Metadata::comment() const
{
    return m_comment;
}

void Metadata::loadMetadata()
{
    if (!m_url.isValid()) {
        return;
    }

    m_openWithApps->clear();
    m_metadata->clear();

    KFormat format;
    KFileItem fileItem(m_url); // TODO file fetch job?

    QString title = fileItem.name();
    // TODO if application use app name

    if (m_title != title) {
        m_title = title;
        emit titleChanged(title);
    }

    if (!fileItem.mimetype().isEmpty()) {
        const auto services = KMimeTypeTrader::self()->query(fileItem.mimetype(), QStringLiteral("Application"));

        for (const auto &service : services) {
            // TODO does QStandardItemModel take ownership?
            auto *item = new QStandardItem(service->name());
            item->setData(service->icon(), Qt::DecorationRole);
            item->setData(service->storageId(), static_cast<int>(OpenWithAppsRoles::StorageId));

            m_openWithApps->appendRow(item);
        }
    }

    auto addMetadataRow = [this](const QString &label, const QString &text) -> QStandardItem* {
        if (text.isEmpty()) {
            return nullptr;
        }

        auto *item = new QStandardItem(label);
        item->setData(text, static_cast<int>(MetadataRoles::Text));
        m_metadata->appendRow(item);
        return item;
    };

    // TODO can we just leverage Baloo FileMetaDataProvider?

    addMetadataRow(i18nc("file type, eg text document", "Type"), fileItem.mimeComment());
    addMetadataRow(i18nc("file last modified date", "Modified"), format.formatRelativeDateTime(
                       fileItem.time(KFileItem::ModificationTime), QLocale::LongFormat));

    // TODO other properties
    // TODO kfilemetadata properties and stuff





}
