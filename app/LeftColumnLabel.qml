import QtQuick 2.2
import QtQuick.Layouts 1.1

import org.kde.plasma.components 2.0 as PlasmaComponents

PlasmaComponents.Label {
    Layout.minimumWidth: Math.round(root.width / 4)
    Layout.maximumWidth: Layout.minimumWidth

    horizontalAlignment: Text.AlignRight
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    opacity: 0.6
}

