import QtQuick 2.2
import QtQuick.Layouts 1.1

import QtQuick.Window 2.2

import QtQuick.Controls 1.1 as QQC

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import org.kde.triage 1.0 as Triage

Item {
    id: root

    property url url

    width: units.gridUnit * 16
    height: 768 // overriden by view

    Triage.Metadata {
        id: metadata
        url: root.url
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            spacing: 0

            PlasmaExtras.Heading {
                Layout.fillWidth: true
                text: metadata.title || i18nc("window title", "Preview")
                elide: Text.ElideRight
            }

            PlasmaComponents.ToolButton {
                iconName: "view-fullscreen"
                tooltip: i18n("Fullscreen")
                onClicked: console.log("TODO")
            }

            PlasmaComponents.ToolButton {
                iconName: "window-close"
                tooltip: i18n("Close")
                onClicked: triageWindow.hide()
            }
        }

        Triage.Preview {
            id: preview
            Layout.fillWidth: true
            Layout.fillHeight: true
            url: root.url
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: root.columnSpacing

            LeftColumnLabel {
                // FIXME if we could use Qt 5.6 we could use "topPadding"
                // to align the label with the apps next of it
                Layout.alignment: Qt.AlignTop
                text: i18n("Open With")
            }

            ListView {
                id: openWithAppsList
                Layout.fillWidth: true
                Layout.preferredHeight: contentHeight
                interactive: false
                currentIndex: -1

                highlight: PlasmaComponents.Highlight {}

                model: metadata.openWithApps

                delegate: MouseArea {
                    width: openWithAppsList.width
                    height: openWithAppItemRow.height + 2 * openWithAppItemRow.y

                    hoverEnabled: true

                    onEntered: openWithAppsList.currentIndex = index
                    onExited: openWithAppsList.currentIndex = -1

                    RowLayout {
                        id: openWithAppItemRow
                        x: units.smallSpacing
                        y: x
                        width: parent.width - 2 * x

                        PlasmaCore.IconItem {
                            source: model.iconName
                        }

                        PlasmaComponents.Label {
                            Layout.fillWidth: true
                            text: model.title
                        }
                    }
                }
            }
        }

        Separator {
            Layout.fillWidth: true
        }

        ColumnLayout {
            Layout.fillWidth: true
            spacing: root.columnSpacing

            Repeater {
                // TODO should we split name and location separate of metadata?
                model: metadata.metadata

                RowLayout {
                    Layout.fillWidth: true

                    LeftColumnLabel {
                        // FIXME if we could use Qt 5.6 we could use "topPadding"
                        // to align the label with the apps next of it
                        Layout.alignment: Qt.AlignTop
                        text: model.title
                    }

                    // FIXME don't create an icon item just because one property or two
                    // might have it
                    PlasmaCore.IconItem {

                    }

                    PlasmaComponents.Label {
                        Layout.fillWidth: true
                        textFormat: Text.PlainText
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        text: model.text
                    }
                }
            }
        }
    }
}
