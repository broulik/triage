import QtQuick 2.2

//import org.kde.plasma.core 2.0 as PlasmaCore
//import org.kde.plasma.components 2.0 as PlasmaComponents

Item {
    height: units.largeSpacing

    // TODO plasma svg?

    Rectangle {
        anchors.centerIn: parent
        width: parent.width
        height: units.devicePixelRatio
        color: theme.textColor
        opacity: 0.1
    }
}
