/*
 *  Copyright 2014 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "view.h"

#include <QGuiApplication>
#include <QDebug>
#include <QQuickItem>
#include <QQmlContext>
#include <QScreen>
#include <QQmlEngine>
#include <QClipboard>
#include <QPlatformSurfaceEvent>

#include <KWindowSystem>
#include <KWindowEffects>
#include <KAuthorized>
//#include <KGlobalAccel>
#include <KLocalizedString>
#include <KDirWatch>
#include <KCrash/KCrash>

#include <KMimeTypeTrader>

#include <kdeclarative/qmlobject.h>

#include <KPackage/Package>
#include <KPackage/PackageLoader>

#include <KIOWidgets/KRun>

#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/registry.h>
#include <KWayland/Client/surface.h>
#include <KWayland/Client/plasmashell.h>

#include "triageadaptor.h"

View::View(QWindow *)
    : PlasmaQuick::Dialog(),
      m_offset(.5),
      m_floating(false),
      m_plasmaShell(nullptr),
      m_plasmaShellSurface(nullptr)
{
    initWayland();
    setClearBeforeRendering(true);
    setColor(QColor(Qt::transparent));
    setFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);

    KCrash::setFlags(KCrash::AutoRestart);

    //m_config = KConfigGroup(KSharedConfig::openConfig(QStringLiteral("krunnerrc")), "General");

    setFreeFloating(true);
    //setFreeFloating(m_config.readEntry("FreeFloating", false));
    //reloadConfig();

    new TriageAdaptor(this);
    QDBusConnection::sessionBus().registerObject(QStringLiteral("/"), this);
    // FIXME we have the class info, so we shouldnt need to tell the service name here
    QDBusConnection::sessionBus().registerService(QStringLiteral("org.kde.triage"));

    /*if (KAuthorized::authorize(QStringLiteral("run_command"))) {
        QAction *a = new QAction(0);
        QObject::connect(a, &QAction::triggered, this, &View::displayOrHide);
        a->setText(i18n("Run Command"));
        a->setObjectName(QStringLiteral("run command"));
        //KGlobalAccel::self()->setDefaultShortcut(a, QList<QKeySequence>() << QKeySequence(Qt::ALT + Qt::Key_Space), KGlobalAccel::NoAutoloading);
        //KGlobalAccel::self()->setShortcut(a, QList<QKeySequence>() << QKeySequence(Qt::ALT + Qt::Key_Space) << QKeySequence(Qt::ALT + Qt::Key_F2) << Qt::Key_Search);

        a = new QAction(0);
        QObject::connect(a, &QAction::triggered, this, &View::displayWithClipboardContents);
        a->setText(i18n("Run Command on clipboard contents"));
        a->setObjectName(QStringLiteral("run command on clipboard contents"));
        KGlobalAccel::self()->setDefaultShortcut(a, QList<QKeySequence>() << QKeySequence(Qt::ALT+Qt::SHIFT+Qt::Key_F2));
        KGlobalAccel::self()->setShortcut(a, QList<QKeySequence>() << QKeySequence(Qt::ALT+Qt::SHIFT+Qt::Key_F2));
    }*/

    m_qmlObj = new KDeclarative::QmlObject(this);
    m_qmlObj->setInitializationDelayed(true);
    connect(m_qmlObj, &KDeclarative::QmlObject::finished, this, &View::objectIncubated);

    /*KPackage::Package package = KPackage::PackageLoader::self()->loadPackage(QStringLiteral("Plasma/LookAndFeel"));
    KConfigGroup cg(KSharedConfig::openConfig(QStringLiteral("kdeglobals")), "KDE");
    const QString packageName = cg.readEntry("LookAndFeelPackage", QString());
    if (!packageName.isEmpty()) {
        package.setPath(packageName);
    }
*/
    //m_qmlObj->setSource(QUrl::fromLocalFile(package.filePath("runcommandmainscript")));
    m_qmlObj->setSource(QUrl::fromLocalFile(QStringLiteral("/home/kaiuwe/Projekte/triage/app/main.qml")));
    m_qmlObj->engine()->rootContext()->setContextProperty(QStringLiteral("triageWindow"), this);
    m_qmlObj->completeInitialization();

    auto screenRemoved = [this](QScreen* screen) {
        if (screen == this->screen()) {
            setScreen(qGuiApp->primaryScreen());
            hide();
        }
    };

    auto screenAdded = [this](QScreen* screen) {
        connect(screen, &QScreen::geometryChanged, this, &View::screenGeometryChanged);
        screenGeometryChanged();
    };

    foreach(QScreen* s, QGuiApplication::screens())
        screenAdded(s);
    connect(qGuiApp, &QGuiApplication::screenAdded, this, screenAdded);
    connect(qGuiApp, &QGuiApplication::screenRemoved, this, screenRemoved);

    connect(KWindowSystem::self(), &KWindowSystem::workAreaChanged, this, &View::resetScreenPos);

    connect(this, &View::visibleChanged, this, &View::resetScreenPos);

   // KDirWatch::self()->addFile(m_config.name());

    // Catch both, direct changes to the config file ...
    connect(KDirWatch::self(), &KDirWatch::dirty, this, &View::reloadConfig);
    connect(KDirWatch::self(), &KDirWatch::created, this, &View::reloadConfig);

    if (m_qmlObj->rootObject()) {
        // TODO event compress
        connect(m_qmlObj->rootObject(), SIGNAL(widthChanged()), this, SLOT(resetScreenPos()));
        //connect(m_qmlObj->rootObject(), SIGNAL(heightChanged()), this, SLOT(resetScreenPos()));
    }

    setLocation(Plasma::Types::RightEdge);

    connect(qGuiApp, &QGuiApplication::focusWindowChanged, this, &View::slotFocusWindowChanged);
}

View::~View()
{
}

QStringList View::openWithAppNames() const
{
    return m_openWithAppNames;
}

void View::openWith(int index)
{
    const auto service = KService::serviceByDesktopPath(m_openWithEntryPaths.at(index));
    if (!service) {
        return;
    }

    KRun::runService(*service, {m_url}, nullptr);//QApplication::activeWindow());
}

void View::initWayland()
{
    if (!QGuiApplication::platformName().startsWith(QLatin1String("wayland"))) {
        return;
    }
    using namespace KWayland::Client;
    auto connection = ConnectionThread::fromApplication(this);
    if (!connection) {
        return;
    }
    Registry *registry = new Registry(this);
    registry->create(connection);
    QObject::connect(registry, &Registry::interfacesAnnounced, this,
        [registry, this] {
            const auto interface = registry->interface(Registry::Interface::PlasmaShell);
            if (interface.name != 0) {
                m_plasmaShell = registry->createPlasmaShell(interface.name, interface.version, this);
            }
        }
    );

    registry->setup();
    connection->roundtrip();
}

void View::objectIncubated()
{
    setMainItem(qobject_cast<QQuickItem *>(m_qmlObj->rootObject()));
}

void View::slotFocusWindowChanged()
{
//    // TODO for debugging
    if (!QGuiApplication::focusWindow()) {
        setVisible(false);
    }
}

bool View::freeFloating() const
{
    return m_floating;
}

void View::setFreeFloating(bool floating)
{
    if (m_floating == floating) {
        return;
    }

    m_floating = floating;
    if (m_floating) {
        setLocation(Plasma::Types::Floating);
    } else {
        setLocation(Plasma::Types::TopEdge);
    }

    positionOnScreen();
}

void View::reloadConfig()
{
    m_config.config()->reparseConfiguration();
    setFreeFloating(m_config.readEntry("FreeFloating", false));
}

bool View::event(QEvent *event)
{
    // QXcbWindow overwrites the state in its show event. There are plans
    // to fix this in 5.4, but till then we must explicitly overwrite it
    // each time.
    const bool retval = Dialog::event(event);
    bool setState = event->type() == QEvent::Show;
    if (event->type() == QEvent::PlatformSurface) {
        if (auto e = dynamic_cast<QPlatformSurfaceEvent*>(event)) {
            setState = e->surfaceEventType() == QPlatformSurfaceEvent::SurfaceCreated;
        }
    }
    if (setState) {
        KWindowSystem::setState(winId(), NET::SkipTaskbar | NET::SkipPager);
    }

    if (m_plasmaShell && event->type() == QEvent::PlatformSurface) {
        if (auto e = dynamic_cast<QPlatformSurfaceEvent*>(event)) {
            using namespace KWayland::Client;
            switch (e->surfaceEventType()) {
            case QPlatformSurfaceEvent::SurfaceCreated: {
                Surface *s = Surface::fromWindow(this);
                if (!s) {
                    return false;
                }
                m_plasmaShellSurface = m_plasmaShell->createSurface(s, this);
                break;
            }
            case QPlatformSurfaceEvent::SurfaceAboutToBeDestroyed:
                delete m_plasmaShellSurface;
                m_plasmaShellSurface = nullptr;
                break;
            }
        }
    } else if (m_plasmaShellSurface && event->type() == QEvent::Move) {
        QMoveEvent *me = static_cast<QMoveEvent *>(event);
        m_plasmaShellSurface->setPosition(me->pos());
    }

    return retval;
}

void View::showEvent(QShowEvent *event)
{
    KWindowSystem::setOnAllDesktops(winId(), true);
    Dialog::showEvent(event);
    positionOnScreen();
    requestActivate();
}

void View::screenGeometryChanged()
{
    if (isVisible()) {
        positionOnScreen();
    }
}

void View::resetScreenPos()
{
    //if (isVisible() && !m_floating) {
        positionOnScreen();
    //}
}

void View::positionOnScreen()
{
    // when we unload the preview we update the pos and will jump somewhere
    if (!isVisible()) {
        return;
    }

    QScreen *shownOnScreen = nullptr;
    if (QGuiApplication::screens().count() <= 1) {
        shownOnScreen = QGuiApplication::primaryScreen();
    } else {
        foreach (QScreen *screen, QGuiApplication::screens()) {
            if (screen->geometry().contains(QCursor::pos(screen))) {
                shownOnScreen = screen;
                break;
            }
        }
    }
    Q_ASSERT(shownOnScreen);

    setScreen(shownOnScreen);
    const QRect r = shownOnScreen->availableGeometry();

    setPosition(r.right() - width() + 1, r.top());
    // take into account this off by one error in QRect?
    setHeight(r.bottom() - r.top());

    KWindowSystem::setOnAllDesktops(winId(), true);
    KWindowEffects::slideWindow(winId(), KWindowEffects::RightEdge, 0);

    //KWindowSystem::forceActiveWindow(winId());
}

void View::preview(const QString &path)
{
    qDebug() << "Show pref" << path;

    // TODO some sanitizing, ie. from local file and what not
    m_url = QUrl(path);

    // TODO dont crash if this is in valid, also, can't we just access our Q_PROPERTYs?
    m_qmlObj->rootObject()->setProperty("url", m_url);

    m_openWithAppNames.clear();
    m_openWithEntryPaths.clear();

    const auto services = KMimeTypeTrader::self()->query(QMimeDatabase().mimeTypeForFile(m_url.toLocalFile()).name(),
                                                         QStringLiteral("Application"));
    // reserve...
    for (const auto &service : services) {
        m_openWithAppNames << service->name();
        m_openWithEntryPaths << service->entryPath();
    }

    // FIXME only emit if actually changed
    emit openWithAppNamesChanged();

    // TODO only show if/when preview is ready
    positionOnScreen();
    show();
}

void View::hidePreview()
{
    qDebug() << "hide";
    hide();
}
